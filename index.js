const express = require('express');
const {json} = require('body-parser');
const {confirmRouter} = require('./confirmRouter');
const {managerRouter} = require('./managerRouter');
const app = express();
const port = 3000;


app.use(json());
app.use("/confirm", confirmRouter);
app.use("/manager", managerRouter);

app.listen(port, () => {
    console.log(`rest server is up in port ${port}`);
});