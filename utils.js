const axios = require('axios');
const dateValidFormat = new RegExp(/\d{4}-\d{2}-\d{2}$/);
const covidApiBaseRoute = "https://covid-api.mmediagroup.fr/v1";
const countryHistoryMap = new Map();
const dayInMilli = 86400000;//24 hours = 24 * 60 minitues = 24 * 60 * 60 seconds = 24 * 60 * 60 * 1000 milisecound = 86400000 miliseconds 

function updateZero(number) {
    return number < 10 ? `0${number}` : number;
} 

//To get date in format YYYY-MM-DD
function getDateFormat(date) {
    return `${date.getFullYear()}-${updateZero(date.getMonth() + 1)}-${updateZero(date.getDate())}`; 
}

function checkIfDateValid(date) {
    return date && !!date.match(dateValidFormat);
}

function checkDateInput(fromDate, toDate) {
    return checkIfDateValid(fromDate) && checkIfDateValid(toDate); 
} 

//get day before given date  
function getDayBefore(someDateFormat) {
    const someDate = new Date(someDateFormat);
    const yesterdayInMilisecond = someDate.getTime() - dayInMilli; 
    const retDayBefore = new Date(yesterdayInMilisecond); 

    //To get date in format YYYY-MM-DD
    return getDateFormat(retDayBefore); 
}

async function sendUrl(url) {
    let result =  await axios.get(url);
    return result.data;
}

//method that return country data if given Aplha-3 code country if exists in restcountries.eu API.
async function getCountryData(countryCode) {
    let retVal;
    if (countryCode) {
        const result = await sendUrl(`https://restcountries.eu/rest/v2/alpha?codes=${countryCode}`);
        if(result[0]) {
            retVal = result[0];
        } else {
            throw new Error(`There is no country with code ${countryCode} in countries API`);
        }
    }

    return retVal;
}

//Check if specific day is currnt day or not and return relevent confirmed/deaths data of selected country 
async function getConfirmedOrDeathsOfSpecificDay(specificDate, countryName, dataType){
    let currentDate = getDateFormat(new Date());
    let isCurrentDay = currentDate === specificDate;
    let currentConfirm;
    let historyDates; 

    if(isCurrentDay){
        let response = await sendUrl(`${covidApiBaseRoute}/cases?country=${countryName}`);
        if(dataType === 'confirmed'){
            currentConfirm = response.All.confirmed;
        } else{ //'deaths' 
            currentConfirm = response.All.deaths; 
        }
    }else {   
        if(!countryHistoryMap.has(countryName)) {
            let result; 
            if(dataType === 'confirmed') {
                result = await sendUrl(`${covidApiBaseRoute}/history?country=${countryName}&status=confirmed`);
            } else { //'deaths' 
                result = await sendUrl(`${covidApiBaseRoute}/history?country=${countryName}&status=deaths`);
            }
            countryHistoryMap.set(countryName, result.All.dates);
        }

        historyDates = countryHistoryMap.get(countryName);
        
    }
    return isCurrentDay ? currentConfirm : historyDates[specificDate];
}

//method that get given country and date and return the number of new confirmed case from covid-api.mmediagroup.fr API 
async function getconfirmedInCovid19(countryName, specificDate, dayBefore) {
    let retConfirmed = -1;
    if (countryName && specificDate && dayBefore) {
        const sourceDayConfirm = await getConfirmedOrDeathsOfSpecificDay(specificDate, countryName, 'confirmed')
        const comfirmDatBefore = await getConfirmedOrDeathsOfSpecificDay(dayBefore, countryName, 'confirmed')
          
        if(sourceDayConfirm && comfirmDatBefore) {
            retConfirmed = sourceDayConfirm - comfirmDatBefore;
        } else {
            throw new Error(`There is no such date ${specificDate} or ${dayBefore} in history data`);
        }
    } else {
        throw new Error(`Country or dates are missing`);
    }
    return retConfirmed;
}

module.exports = {
    updateZero,
    getDateFormat,
    checkIfDateValid,
    checkDateInput,
    getDayBefore,
    sendUrl, 
    getCountryData,
    getConfirmedOrDeathsOfSpecificDay,
    getconfirmedInCovid19
}