const express = require('express');
const confirmRouter = express.Router(); 
const utils = require('./utils');
const dayInMilli = 86400000;//24 hours = 24 * 60 minitues = 24 * 60 * 60 seconds = 24 * 60 * 60 * 1000 milisecound = 86400000 miliseconds

confirmRouter.get('/confirmcaseincountry', async (req, res) => {
    //get params from user 
    const countryCode = req.query.country;
    const specificDate = req.query.chosenDate;
    
    let resonseContent = "Missing";
    let resonseStatus = 200; 
    const isValidDate = utils.checkIfDateValid(specificDate);

    if (countryCode && specificDate && isValidDate) {
        try{
            const country = await utils.getCountryData(countryCode);
            if(country) { 
                resonseContent = await utils.getconfirmedInCovid19(country.name, specificDate, utils.getDayBefore(specificDate));
            }
        }
        catch(error) {
            resonseStatus = 500;
            resonseContent = `Error: ${error.message}`;
        }
    } else {
        if(!isValidDate) {
            resonseStatus = 400;
            resonseContent = `chosen date is invalid params`;
        } else {
            resonseStatus = 406;
            resonseContent = `Missing country and chosen date in query params`;
        }
    }
    res.status(resonseStatus).send(resonseContent.toString());
})

confirmRouter.get('/confirmcaestwocountries', async (req, res) => { 
    //get params from user 
    const sourceCountry = req.query.sourceCountry;
    const targetCountry = req.query.targetCountry;
    const fromDate = req.query.fromDate;
    const toDate = req.query.toDate;

    let arrayOfPercents = [];
    let resonseStatus = 200; 
    let validDatesInput = utils.checkDateInput(fromDate, toDate);

    if (sourceCountry && targetCountry && validDatesInput) {
        try{
            const sourceCountryData = await utils.getCountryData(sourceCountry);
            const targetCountryData = await utils.getCountryData(targetCountry);
            if(sourceCountryData && targetCountryData) { 
                let fromDateInMili = new Date(fromDate).getTime();
                let ToDateInMili = new Date(toDate).getTime();

                while(ToDateInMili >= fromDateInMili) {
                    let date = utils.getDateFormat(new Date(ToDateInMili)); 
                    let confirmSourceCountry = await utils.getConfirmedOrDeathsOfSpecificDay(date,sourceCountryData.name, 'confirmed');
                    let confirmTargetCountry = await utils.getConfirmedOrDeathsOfSpecificDay(date,targetCountryData.name, 'confirmed')
                    let percent = confirmSourceCountry/sourceCountryData.population - confirmTargetCountry/targetCountryData.population;
                    
                    //insert percent to result array 
                    arrayOfPercents.push(Number((percent)).toFixed(3)); 
                    //minus day In Millisecounds 
                    ToDateInMili = ToDateInMili - dayInMilli; 
                }
            }
        }
        catch(error) {
            resonseStatus = 500;
        }
    } else {
        if(!validDatesInput) {
            resonseStatus = 400;
        } else {
            resonseStatus = 406;
        }
    }

    res.status(resonseStatus).send(JSON.stringify(arrayOfPercents));
})

module.exports = {
    confirmRouter
}