const express = require('express');
const managerRouter = express.Router();
const utils = require('./utils');
const countrieslist  = new Set(); // set is not allowed duplicates    


managerRouter.get('/addcountry', async (req, res) => {
    //get param from user 
    const countryCode = req.query.country;
    
    let resonseContent = "Missing";
    let resonseStatus = 200; 

    if (countryCode) {
        try{
            const country = await utils.getCountryData(countryCode);
            if(country) { 
                //insert name of country to result array 
                countrieslist.add(country.name);
                resonseContent = `Country ${country.name} added successefully`;  
            }
        }catch(error) {
            resonseStatus = 500;
            resonseContent = `Error: ${error.message}`;
        }
    } else {
            resonseStatus = 406;
            resonseContent = `Missing country code in query params`;
    }
    res.status(resonseStatus).send(resonseContent.toString());
})

managerRouter.get('/removecountry', async (req, res) => {
    //get param from user 
    const countryName = req.query.countryName;
    
    let resonseContent = "Missing";
    let resonseStatus = 200; 

    if (countryName) {
        try{
            if(countrieslist.has(countryName)) {
                //remove country from result set
                countrieslist.delete(countryName); 
                resonseContent = `Country ${countryName} removed successefully`; 
            } else {
                throw new Error(`Country ${countryName} is not in the list, therefore not removed`);  
            }
        }catch(error) {
            resonseStatus = 500;
            resonseContent = `Error: ${error.message}`;
        }
    } else {
            resonseStatus = 406;
            resonseContent = `Missing country code in query params`;
    }
    res.status(resonseStatus).send(resonseContent.toString());
})

managerRouter.get('/listofcountries', async (req, res) => {  
    let resonseStatus = 200;
    //Change Set to array - let temp = [...mySet]
    const countriesArray = Array.from(countrieslist) 
    res.status(resonseStatus).send(JSON.stringify(countriesArray));
})

managerRouter.get('/numberofdeaths', async (req, res) => { 
    //get params from user 
    const fromDate = req.query.fromDate;
    const toDate = req.query.toDate;

    let arrayOfDeaths = new Set(new Map());
    let resonseStatus = 200; 
    let validDatesInput = utils.checkDateInput(fromDate, toDate);
    const allPromise = [];

    if (validDatesInput) {
        try{
            for (let countryName of countrieslist.values()) {
                let fromDateInMili = new Date(fromDate).getTime();
                let ToDateInMili = new Date(toDate).getTime();

                while(ToDateInMili >= fromDateInMili) {
                        let date = utils.getDateFormat(new Date(ToDateInMili)); 
                        
                        //instead do: await utils.getConfirmedOrDeathsOfSpecificDay(date,countryData.name, 'deaths') => used to push promise into array
                        allPromise.push(utils.getConfirmedOrDeathsOfSpecificDay(date,countryName, 'deaths')) 
                        
                        //insert into result array 
                        arrayOfDeaths.add(countryName, [date,deathsInCountry]); 
                        //minus day In Millisecounds 
                        ToDateInMili = ToDateInMili - dayInMilli; 
                }
            }
            try {
                await Promise.all(allPromise);
            }
            catch{
                resonseStatus = 500;
            }  
        }
        catch(error) {
            resonseStatus = 500;
        }
    } else {
        if(!validDatesInput) {
            resonseStatus = 400;
        } else {
            resonseStatus = 406;
        }
    }
    res.status(resonseStatus).send(JSON.stringify(arrayOfDeaths));
})

module.exports = {
    managerRouter
}